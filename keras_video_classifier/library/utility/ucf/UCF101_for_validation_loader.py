import urllib.request
import os
import sys
import patoolib
from keras_video_classifier.library.utility.ucf import UCF101_loader
import shutil
from sklearn.model_selection import train_test_split


def scan_ucf(data_dir_path, limit):
    input_data_dir_path = data_dir_path + '/UCF-101-validation'

    result = dict()

    dir_count = 0
    for f in os.listdir(input_data_dir_path):
        file_path = input_data_dir_path + os.path.sep + f
        if not os.path.isfile(file_path):
            dir_count += 1
            for ff in os.listdir(file_path):
                video_file_path = file_path + os.path.sep + ff
                result[video_file_path] = f
        if dir_count == limit:
            break
    return result


def scan_ucf_with_labels(data_dir_path, labels):
    input_data_dir_path = data_dir_path + '/UCF-101-validation'

    result = dict()

    dir_count = 0
    for label in labels:
        file_path = input_data_dir_path + os.path.sep + label
        if not os.path.isfile(file_path):
            dir_count += 1
            for ff in os.listdir(file_path):
                video_file_path = file_path + os.path.sep + ff
                result[video_file_path] = label
    return result


def split_ucf_validation(data_dir_path):
    UCF101_data_dir_path = data_dir_path + "/UCF-101-validation"
    if not os.path.exists(UCF101_data_dir_path):
        os.makedirs(UCF101_data_dir_path)
        ucf_dic = UCF101_loader.scan_ucf(data_dir_path, sys.maxsize)
        file_list = sorted(list(ucf_dic.keys()))
        train_file, test_file = train_test_split(file_list, test_size=0.1, random_state=42)
        for file_path in test_file:
            dst_path = os.path.join(UCF101_data_dir_path, ucf_dic[file_path])
            os.makedirs(dst_path, exist_ok=True)
            shutil.move(file_path, dst_path)


def main():
    data_dir_path = './demo/very_large_data'
    split_ucf_validation(data_dir_path)


if __name__ == '__main__':
    main()
