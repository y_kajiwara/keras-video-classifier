import numpy as np
from keras import backend as K
import sys
import os
from sklearn.model_selection import train_test_split
import cv2


def main():
    K.set_image_data_format('channels_last')
    sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

    from keras_video_classifier.library.recurrent_networks import VGG16LSTMVideoClassifier
    from keras_video_classifier.library.utility.ucf import UCF101_loader, UCF101_for_validation_loader

    vgg16_include_top = False
    data_dir_path = os.path.join(os.path.dirname(__file__), 'very_large_data')
    model_dir_path = os.path.join(os.path.dirname(__file__), 'models', 'UCF-101')
    config_file_path = VGG16LSTMVideoClassifier.get_config_file_path(model_dir_path,
                                                                     vgg16_include_top=vgg16_include_top)
    weight_file_path = VGG16LSTMVideoClassifier.get_weight_file_path(model_dir_path,
                                                                     vgg16_include_top=vgg16_include_top)

    np.random.seed(42)

    UCF101_loader.load_ucf(data_dir_path)
    UCF101_for_validation_loader.split_ucf_validation(data_dir_path)

    predictor = VGG16LSTMVideoClassifier()
    predictor.load_model(config_file_path, weight_file_path)

    videos = UCF101_for_validation_loader.scan_ucf_with_labels(data_dir_path, [label for (label, label_index) in predictor.labels.items()])
    # videos = {'/Users/yoshiyuki/Desktop/whiteboard3.mp4': 'test'}
    video_file_path_list = np.array([file_path for file_path in videos.keys()])
    np.random.shuffle(video_file_path_list)

    for video_file_path in video_file_path_list:
        label = videos[video_file_path]
        duration = 4
        pad = 2
        predicted_label = predictor.test(video_file_path, duration, pad)
        print('predicted: ' + str(predicted_label) + ' actual: ' + label)
        cap = cv2.VideoCapture(video_file_path)
        fps = cap.get(cv2.CAP_PROP_FPS)
        ret, frame = cap.read()
        key = 0
        sum = 0
        while True:
            ret, frame = cap.read()
            if ret:
                pred_idx = int((sum / fps) / pad)
                cv2.putText(frame, 'predicted: {}'.format(predicted_label[pred_idx]), (30, 30), cv2.FONT_HERSHEY_PLAIN, 1,
                            (255, 255, 0))
                cv2.putText(frame, 'actual: {}'.format(label), (30, 50), cv2.FONT_HERSHEY_PLAIN, 1,
                            (255, 0, 255))
                cv2.imshow('movie', frame)
                key = cv2.waitKey(int(1000/fps))
                sum += 1
                if key == 27:
                    break
            else:
                break
        if key == 27:
            break


if __name__ == '__main__':
    main()
